#!/usr/bin/python2.5

import os

import simplejson
from colubrid        import BaseApplication, HttpResponse, execute
from colubrid.server import StaticExports
from paste           import reloader
reloader.install()


class HyenaApplication(BaseApplication):

    def process_request(self):
        if self.request.environ['REQUEST_METHOD'] == 'POST':
            # write the JSON
            self.write_json(self.request.form['json'])
            return HttpResponse('OK')
        else:
            # read the JSON
            response = HttpResponse(self.read_json())
            response['Content-Type'] = 'text/plain' # 'text/x-json'
            return response

    def write_json(self, json):
        open('data.json', 'w').write(json)

    def read_json(self):
        json = self.read_json_from_disk()
        data = simplejson.loads(json)
        data['fortune'] = os.popen('fortune').read().replace('\n', '<br />')
        return simplejson.dumps(data)

    def read_json_from_disk(self):
        if os.path.isfile('data.json'):
            return open('data.json').read()
        
        # defaul json
        return simplejson.dumps({
            'name': 'srid',
            'actions': [
                {'text': 'get my sansonite zip repaired under warranty',
                 'type': 'errand'},
                {'text': 'guide dushyant on django templates',
                 'type': 'work'},
                {'text': 'm$ reimbursement',
                 'type': 'errand'}
                ]
            })

app = HyenaApplication
app = StaticExports(app, {
        '/app': './app',
        })

if __name__ == '__main__':
    execute(app)
