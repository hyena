/*
 * Hyena - personal portal system
 */

$(document).ready(function(){

  function syncTo(data){
	var json = JSON.stringify(data);
	$.post('/', {json: json}, function(data, textStatus){
	  console.debug('sync %o', data);
	});
  }

  function removeAction(data, text){
	data.actions = $.grep(data.actions, function(action){
	  return (action.text != text);
	});
  }

  function addAction(data, action){
	data.actions.push(action);
  } 

  function render(data){
	gdata = data; // debug
	
	var html = TrimPath.processDOMTemplate('t-home', data);
	$('#site').html(html);

	$('.na.delete').click(function(){
	  console.debug('ui: delete clicked');
	  var div = $(this).parent('div.na');
	  var na  = div.find('.text');
	  removeAction(data, na.text());
	  div.fadeOut(function(){$(this).remove();});
	  syncTo(data);
	  $('#na-input').focus();
	  return false;
	});

	$('#na-input').keypress(function (e){
	  if (e.which == 13){
		console.log('enter');
		console.debug(this);
		addAction(data, {
		  text: this.value,
		  type: 'errand',
		  date: Date.parse('next thursday')
		});
		syncTo(data);
		render(data);
		$('#na-input').focus();
	  }
	});

	$('#site').append(TrimPath.processDOMTemplate('t-books'));
  };
  
  $.getJSON('/', render);
});